ZeroOneKnapSack.java

Type
Java
Size
1 KB (1,085 bytes)
Storage used
1 KB (1,085 bytes)
Location
Algorithm Lab
Owner
me
Modified
Jul 12, 2017 by me
Opened
8:14 AM by me
Created
Jul 12, 2017 with Google Drive Web
Add a description
Viewers can download
import java.util.Scanner;

public class ZeroOneKnapSack {

	static int max(int a, int b){
		return (a > b)? a : b;
	}

	static int KnapSack(int W, int wt[], int val[], int n){
		int i, j;
		int k[][] = new int[n+1][W+1];

		for(i=0;i<=n;++i){
			for(j=0;j<=W;++j){
				if(i==0 || j==0)
					k[i][j]=0;
				else if(wt[i-1]<=j)
					k[i][j] = max(val[i-1]+k[i-1][j-wt[i-1]], k[i-1][j]);
				else
					k[i][j]=k[i-1][j];
			}
		}
		return k[n][W];
	}

	public static void main(String args[]){

		Scanner in = new Scanner(System.in);

		System.out.println("Enter no. of items: ");
		int n = in.nextInt();

		System.out.println("Enter the weights of the items: ");

		int wt[] = new int[n];

		for(int i=0;i<n;++i)
			wt[i] = in.nextInt();

		System.out.println("Enter the values of the items: ");

		int val[] = new int[n];

		for(int i=0;i<n;++i)
			val[i] = in.nextInt();

		System.out.println("Enter Knapsack weight: ");
		int W = in.nextInt();

		System.out.println("Maximum profit is: " + KnapSack(W,wt,val,n));
		in.close();
	}
}